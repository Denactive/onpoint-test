// ///////////////////////////////// //
//
//          Globals
//
// ///////////////////////////////// //

// set to true

const modalsDebug = true;
const routerDebug = true;
const linkControllerDebug = true;
const viewsDebug = true;
const pageDebug = true;

export {modalsDebug, routerDebug, linkControllerDebug, viewsDebug, pageDebug};
