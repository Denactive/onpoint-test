import BaseComponent from '../_basic/baseComponent.js';
import CarouselContentView from './CarouselContentView.js';

import Slide1 from '../slide1/slide1.js';
import Slide2 from '../slide2/slide2.js';
import Slide3 from '../slide3/slide3.js';

/**
 * @class CarouselContent
 */
export default class CarouselContent extends BaseComponent {
  constructor() {
    super();
    this.view = new CarouselContentView();
    this.slide1 = new Slide1();
    this.slide2 = new Slide2();
    this.slide3 = new Slide3();
    this.currentSlide = 1;
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @return {HTMLElement}
   */
  render() {
    super.render();
    this.root = this.view.render();
    if (!this.keyElems) {
      return document.createElement('div');
    }

    this.keyElems.slide1.appendChild(
        this.slide1.render(() => this.moveToSlide(2)),
    );
    this.keyElems.slide2.appendChild(this.slide2.render());
    this.keyElems.slide3.appendChild(this.slide3.render());

    this.keyElems.rightBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.moveToSlide(this.currentSlide < this.view.slidesNum ?
          (this.currentSlide + 1) : 1);
    });

    this.keyElems.leftBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.moveToSlide(this.currentSlide > 1 ?
          (this.currentSlide - 1) : this.view.slidesNum);
    });

    if (this.keyElems.nav) {
      this.keyElems.indicators.forEach((indicator) => {
        indicator.addEventListener('click', (e) => {
          e.preventDefault();
          this.moveToSlide(e.currentTarget.dataset.indicates);
        });
      });
    }
    return this.root;
  }

  moveToSlide(to) {
    if (!this.keyElems) {
      console.warn(
          '{CarouselContent} component hasn\'t been rendered yet',
      );
      return;
    }

    if (to < 1 || to > this.view.slidesNum) {
      to = 1;
    }

    const nextSlide = this.keyElems[`slide${to}`];
    const transiteLength = nextSlide.style.left;
    // console.warn(transiteLength,  `translateX(-${transiteLength}`);

    this.keyElems.track.style.transform = `translateX(-${transiteLength})`;

    this.keyElems.indicators.forEach((indicator) => {
      indicator.classList.remove('carousel__indicator__fill');
      if (indicator.dataset.indicates <= to) {
        indicator.classList.add('carousel__indicator__fill');
      }
    });
    this.currentSlide = to;
  }
}
