import BaseComponentView from '../_basic/baseComponentView.js';
import carouselContentComponent from './carouselContent.pug.js';

import './carouselContent.scss';

/**
 * @class CarouselContentView
 */
export default class CarouselContentView extends BaseComponentView {
  constructor() {
    super();
    this.slidesNum = 0;
  }

  /**
    * @return {HTMLElement}
    */
  render() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = carouselContentComponent();
    const slides = [...wrapper.querySelectorAll('ul.carousel__slides>li')];
    const track = wrapper.querySelector('.carousel__slides');
    const leftBtn = wrapper.querySelector('.carousel__button--left');
    const rightBtn = wrapper.querySelector('.carousel__button--right');

    const nav = wrapper.querySelector('.carousel__nav');
    const indicators = [...wrapper.querySelectorAll('.carousel__indicator')];

    if (slides.length ===0 ||
        nav ^ indicators ||
        !(track &&
        leftBtn &&
        rightBtn
        )) {
      console.warn(
          '{CarouselContentView} component template contains an error',
      );
      return document.createElement('div');
    }

    this.keyElems = {
      track,
      leftBtn,
      rightBtn,
      nav,
      indicators,
    };

    // хитрым способом определяю ширину слайда, которую буду соблюдать.
    const tmp = document.createElement('div');
    tmp.appendChild(slides[0].cloneNode());
    document.querySelector('#root').appendChild(tmp);
    const width = tmp.getBoundingClientRect().width;
    document.querySelector('#root').removeChild(tmp);

    slides.forEach((el, i) => {
      el.style.left = width * i + 'px';
      el.dataset.slide = i + 1 + '';
      // slide1 : <div class='slide1'>...</div>
      Object.assign(this.keyElems, {[`slide${i + 1}`]: el});
    });
    // slides.forEach((el, i) => console.warn(el.style.left));

    this.slidesNum = slides.length;

    if (nav) {
      if (indicators.length !== slides.length) {
        console.warn(
            '{CarouselContentView}' +
            'indicators number does not match slides number',
        );
      }
      indicators.forEach((el, i) => {
        el.dataset.indicates = i + 1 + '';
        // indicator1 : <div class='indicator1'>...</div>
        Object.assign(this.keyElems, {[`indicator${i + 1}`]: el});
      });
    }

    // console.log(this.keyElems);

    return wrapper.firstChild;
  }
}
