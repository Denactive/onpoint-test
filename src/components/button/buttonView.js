import BaseComponentView from '../_basic/baseComponentView.js';
import buttonComponent from './button.pug.js';

import './button.scss';

/**
 * @class buttonView
 */
export default class ButtonView extends BaseComponentView {
  /**
   * @param {string} char
   * @param {string} sign
   * @return {HTMLElement}
   */
  render(char, sign) {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = buttonComponent({char, sign});

    if (!(true)) {
      console.warn(
          '{CarouselContent} component template contains an error',
      );
      return document.createElement('div');
    }
    this.keyElems = {};

    return wrapper.firstChild;
  }
}
