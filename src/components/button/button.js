import BaseComponent from '../_basic/baseComponent.js';
import ButtonView from './buttonView.js';

/**
 * @class Button
 */
export default class Button extends BaseComponent {
  constructor() {
    super();
    this.view = new ButtonView();
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @param {string} char
   * @param {string} sign
   * @param {callback} onClick
   * @return {HTMLElement}
   */
  render(char, sign, onClick) {
    super.render();
    this.root = this.view.render(char, sign);
    if (!this.keyElems) {
      return document.createElement('div');
    }

    this.root.addEventListener('click', onClick);

    return this.root;
  }
}
