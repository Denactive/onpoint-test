import BaseComponentView from '../_basic/baseComponentView.js';
import slide3Component from './slide3.pug.js';

import './slide3.scss';

/**
 * @class Slide3View
 */
export default class Slide3View extends BaseComponentView {
  /**
    * @return {HTMLElement}
    */
  render() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = slide3Component();

    const btnDiv = wrapper.querySelector('#slide3-place-for-btn');

    if (!true) {
      console.warn(
          '{CarouselContent} component template contains an error',
      );
      return document.createElement('div');
    }
    this.keyElems = {
      btnDiv,
    };

    return wrapper.firstChild;
  }
}
