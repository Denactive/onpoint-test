import BaseComponent from '../_basic/baseComponent.js';
import Slide3View from './slide3View.js';

import Button from '../button/button.js';
import AppModal from '../modal/appModal.js';

import CarouselSlide3 from './carouselSlide3/carouselSlide3.js';
import modalContentComponent from './modalContent.pug.js';

/**
 * @class Slide3
 */
export default class Slide3 extends BaseComponent {
  constructor() {
    super();
    this.view = new Slide3View();
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @return {HTMLElement}
   */
  render() {
    super.render();
    this.root = this.view.render();
    if (!this.keyElems) {
      return document.createElement('div');
    }

    const content = document.createElement('div');
    content.innerHTML = modalContentComponent();
    content.querySelector('.slide3__modal__content')
        .appendChild(new CarouselSlide3().render());

    AppModal.setContent(content.firstChild);
    this.keyElems.btnDiv.appendChild(
        new Button().render('+', 'Подробнее', () => AppModal.open()),
    );

    return this.root;
  }
}
