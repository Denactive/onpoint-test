import BaseComponentView from '../../_basic/baseComponentView.js';
import carouselSlide3Component from './carouselSlide3.pug.js';

/**
 * @class CarouselSlide3View
 */
export default class CarouselSlide3View extends BaseComponentView {
  constructor() {
    super();
    this.slidesNum = 0;
  }

  /**
    * @return {HTMLElement}
    */
  render() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = carouselSlide3Component();
    const slides = [
      ...wrapper.querySelectorAll('.slide3__modal__content__slide'),
    ];
    const leftBtn = wrapper.querySelector('.carousel__button--left');
    const rightBtn = wrapper.querySelector('.carousel__button--right');

    const nav = wrapper.querySelector('.carousel__nav');
    const indicators = [...wrapper.querySelectorAll('.carousel__indicator')];

    if (slides.length ===0 ||
        nav ^ indicators ||
        !(leftBtn && rightBtn
        )) {
      console.warn(
          '{CarouselSlide3View} component template contains an error',
      );
      return document.createElement('div');
    }

    this.keyElems = {
      leftBtn,
      rightBtn,
      nav,
      indicators,
    };

    slides.forEach((el, i) => {
      el.dataset.slide = i + 1 + '';
      Object.assign(this.keyElems, {[`slide${i + 1}`]: el});
    });

    this.slidesNum = slides.length;

    if (indicators.length !== slides.length) {
      console.warn(
          '{CarouselSlide3View} indicators number does not match slides number',
      );
    }
    indicators.forEach((el, i) => {
      el.dataset.indicates = i + 1 + '';
      Object.assign(this.keyElems, {[`indicator${i + 1}`]: el});
    });

    return wrapper.firstChild;
  }
}
