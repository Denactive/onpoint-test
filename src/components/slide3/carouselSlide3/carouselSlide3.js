import BaseComponent from '../../_basic/baseComponent.js';
import CarouselSlide3View from './CarouselSlide3View.js';

/**
 * @class CarouselSlide3
 */
export default class CarouselSlide3 extends BaseComponent {
  constructor() {
    super();
    this.view = new CarouselSlide3View();
    this.currentSlide = 1;
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @return {HTMLElement}
   */
  render() {
    super.render();
    this.root = this.view.render();
    if (!this.keyElems) {
      return document.createElement('div');
    }

    this.keyElems.rightBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.moveToSlide(this.currentSlide < this.view.slidesNum ?
        (this.currentSlide + 1) : 1);
    });

    this.keyElems.leftBtn.addEventListener('click', (e) => {
      e.preventDefault();
      this.moveToSlide(this.currentSlide > 1 ?
        (this.currentSlide - 1) : this.view.slidesNum);
    });

    this.keyElems.indicators.forEach((indicator) => {
      indicator.addEventListener('click', (e) => {
        e.preventDefault();
        this.moveToSlide(e.currentTarget.dataset.indicates);
      });
    });
    return this.root;
  }

  moveToSlide(to) {
    if (!this.keyElems) {
      console.warn(
          '{CarouselSlide3} component hasn\'t been rendered yet',
      );
      return;
    }

    if (to < 1 || to > this.view.slidesNum) {
      to = 1;
    }

    const currentSlide = this.keyElems[`slide${this.currentSlide}`];
    const nextSlide = this.keyElems[`slide${to}`];

    currentSlide.classList.remove('slide3__modal__content__slide--current');
    nextSlide.classList.add('slide3__modal__content__slide--current');

    this.keyElems.indicators.forEach((indicator) => {
      indicator.classList.remove('carousel__indicator__fill');
      if (indicator.dataset.indicates === to + '') {
        indicator.classList.add('carousel__indicator__fill');
      }
    });
    this.currentSlide = to;
  }
}
