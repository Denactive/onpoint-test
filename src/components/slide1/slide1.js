import BaseComponent from '../_basic/baseComponent.js';
import Slide1View from './Slide1View.js';

import Button from '../button/button.js';

/**
 * @class Slide1
 */
export default class Slide1 extends BaseComponent {
  constructor() {
    super();
    this.view = new Slide1View();
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @param {callback?} moveRight
   * @return {HTMLElement}
   */
  render(moveRight = () => {}) {
    super.render();
    this.root = this.view.render();
    if (!this.keyElems) {
      return document.createElement('div');
    }

    this.keyElems.btnDiv.appendChild(
        new Button().render('→', 'Что дальше?', moveRight),
    );

    return this.root;
  }
}
