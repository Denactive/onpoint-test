import BaseComponentView from '../_basic/baseComponentView.js';
import slide1Component from './slide1.pug.js';

import './slide1.scss';

/**
 * @class Slide1View
 */
export default class Slide1View extends BaseComponentView {
  /**
    * @return {HTMLElement}
    */
  render() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = slide1Component();

    const btnDiv = wrapper.querySelector('#slide1-place-for-btn');

    if (!(true)) {
      console.warn(
          '{CarouselContent} component template contains an error',
      );
      return document.createElement('div');
    }
    this.keyElems = {
      btnDiv,
    };

    return wrapper.firstChild;
  }
}
