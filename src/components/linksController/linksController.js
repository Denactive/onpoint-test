// ///////////////////////////////// //
//
//     Определяет взаимодействие
//     со ссылками, но не с
//     переходами по страницам
//
// ///////////////////////////////// //
const configuration = {};
//   template: {
//     action: () => {

//     },
//     props: null,
//   }

// ///////////////////////////////// //
//
//     Общий глобальный обработчик
//     действует только на ссылки
//
// ///////////////////////////////// //

import {linkControllerDebug} from '../../globals.js';

/**
 * Общий глобальный обработчик. Действует только на ссылки.
 * Здесь можно задать любое поведение при клике на HTMLAnchorElement
 * @param {event} event
 */
function linksControllerClickHandler(event) {
  let {target} = event;

  // перебираем вверх, пока не event.currentTarget (root)
  while (target.parentElement !== event.currentTarget) {
    if (linkControllerDebug && target instanceof HTMLAnchorElement) {
      console.log(
          '[linkController] propagation:', {target},
          `${target.dataset['lc_command'] ?
              `command: ${target.dataset['lc_command']}, `: ''}` +
          `${target.dataset['lc_section'] || 'no lc_section'}`,
      );
    }

    if (target.dataset['lc_command'] === 'ignore') {
      return;
    }

    // проверям, что клик был по ссылке (anchor)
    if (target instanceof HTMLAnchorElement) {
      event.preventDefault();

      const props = configuration[target.dataset['lc_section']]?.props;
      const action = configuration[target.dataset['lc_section']]?.action;
      if (typeof action === 'function') {
        action.call(null, props);
      }

      if (target.dataset['lc_command'] !== 'continue') {
        return;
      }
    }

    target = target.parentElement;
  }
}

/**
 * Отвечает за поведение при клике на ссылку,
 * которое не ведет к переходу на другую страницу.
 * HTMLAnchor должен содержать атрибут:
 * data-lc_section="<name>"   - чтобы выполнить действие,
 * записанное под секцией <name>
 * data-lc_command="ignore"   - чтобы игнорировать
 * data-lc_command="continue" - чтобы не прерывать всплытие.
 * @class LinksController
 */
export default class LinksController {
  /**
   * @param {HTMLElement} root
   */
  constructor(root) {
    this.root = root;
    this.enabled = false;
  }

  /**
   * Регистрация элемента в контроллере.
   * section - значение атрибута data-lc-section соответствующего ссылки.
   * action - действие, выполняемое при клике на ссылку с этим атрибутом.
   * props - параметры, передаваемые в action.
   * @param {string} section
   * @param {function} action
   * @param {any[]} props
   * @return {LinksController}
   */
  register(section, action = () => {}, props = null) {
    configuration[section] = {
      action,
      props,
    };
    return this;
  }

  /**
   * Активирует обработчик кликов на элементе root
   */
  enable() {
    if (!this.enabled) {
      this.enabled = true;
      this.root.addEventListener('click', linksControllerClickHandler);
    }
  }

  /**
   * Дизактивирует обработчик кликов на элементе root
   */
  disable() {
    if (this.enabled) {
      this.enabled = false;
      this.root.removeEventListener('click', linksControllerClickHandler);
    }
  }
}
