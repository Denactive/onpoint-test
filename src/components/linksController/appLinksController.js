import LinksController from './linksController.js';

const AppLinksController = new LinksController(document.querySelector('#root'));
AppLinksController.register(
    'to-main',
    () => console.warn('[MainPage] carousel hasn\'t rendered yet'),
);

export default AppLinksController;
