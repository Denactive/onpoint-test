import BaseComponent from '../_basic/baseComponent.js';
import Slide2View from './slide2View.js';

/**
 * @class Slide2
 */
export default class Slide2 extends BaseComponent {
  constructor() {
    super();
    this.view = new Slide2View();
  }

  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @return {HTMLElement}
   */
  render() {
    super.render();
    this.root = this.view.render();
    if (!this.keyElems) {
      return document.createElement('div');
    }

    return this.root;
  }
}
