import BaseComponentView from '../_basic/baseComponentView.js';
import slide2Component from './slide2.pug.js';

import './slide2.scss';

/**
 * @class Slide2View
 */
export default class Slide2View extends BaseComponentView {
  /**
    * @return {HTMLElement}
    */
  render() {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = slide2Component();

    if (!(true)) {
      console.warn(
          '{Slide2} component template contains an error',
      );
      return document.createElement('div');
    }
    this.keyElems = {};

    return wrapper.firstChild;
  }
}
