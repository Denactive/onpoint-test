import ModalView from './modalView.js';


const animationTime = 200;

/**
 * @class Modal
 */
export default class Modal {
  get keyElems() {
    return this.view.keyElems;
  }

  /**
   * Перерисовка подконтрольного элемента
   * @param {string} title
   * @param {HTMLElement} content
   * @param {string?} docTitle
   * @return {HTMLElement}
   */
  constructor(title, content, docTitle = '') {
    this.view = new ModalView();
    this.root = document.getElementById('root');
    this.isClosing = false;
    this.initialDocTitle = '';
    this.docTitle = docTitle;

    this.root
        .appendChild(this.view.render(title, content.outerHTML));
    if (!this.keyElems) {
      return document.createElement('div');
    }
  }

  open() {
    if (!this.keyElems) {
      console.warn(
          '{Modal} component hasn\'t been rendered yet',
      );
      return;
    }
    // во время анимации закрытия не сетим анимацию закрытия
    if (this.isClosing) {
      console.warn('[Modal] open: modal is in closing process');
      return;
    }

    this.keyElems.modalOverlay.classList.remove('modal_close');
    this.keyElems.modalWindow.classList.remove('modal_close');
    this.keyElems.modalOverlay.classList.add('modal_open');
    this.keyElems.modalWindow.classList.add('modal_open');

    this.initialDocTitle = document.title;
    if (this.docTitle) {
      document.title = this.docTitle;
    }
  }

  close() {
    if (!this.keyElems) {
      console.warn(
          '{Modal} component hasn\'t been rendered yet',
      );
      return;
    }

    this.isClosing = true;
    this.keyElems.modalOverlay.classList.add('modal_close');
    this.keyElems.modalWindow.classList.add('modal_close');
    this.keyElems.modalOverlay.classList.remove('modal_open');
    this.keyElems.modalWindow.classList.remove('modal_open');
    setTimeout(() => {
      this.isClosing = false;
    }, animationTime);
    document.title = this.initialDocTitle;
  }
}
