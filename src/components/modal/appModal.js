import Modal from './modal.js';

let modal = null;

/**
 * @class AppModal
 */
class AppModal {
  /**
   * активация модального окна приложения
   */
  static init() {
    if (modal === null) {
      modal = new Modal('', '', 'Brand XY');
      document.getElementById('root').addEventListener('click', (e) => {
        const target = e.target;
        if (target.dataset['modalClose']) {
          e.preventDefault();
          this.close();
        }
      });
    }
  }

  /**
   * открыть модальное окно приложения
   */
  static open() {
    if (modal !== null) {
      modal.open();
    }
  }

  /**
   * закрыть модальное окно приложения
   */
  static close() {
    if (modal !== null) {
      modal.close();
    }
  }

  /**
   * @param {HTMLElement} html
   * задать содержимое попапа. Старое сотрется.
   */
  static setContent(html) {
    if (modal !== null) {
      modal.keyElems.modalWindow.innerHtml = '';
      modal.keyElems.modalWindow.appendChild(html);
    }
  }
}

export default AppModal;
