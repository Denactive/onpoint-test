import modalComponent from './modal.pug.js';

import './modal.scss';

/**
 * @class ModalView
 */
export default class ModalView {
  /**
   * @param {string} title
   * @param {HTMLElement} content
   * @return {HTMLElement}
   */
  render(title, content) {
    const wrapper = document.createElement('div');
    wrapper.innerHTML = modalComponent({title, content});

    const modalOverlay = wrapper.querySelector('.modal__overlay');
    const modalWindow = wrapper.querySelector('.modal__window');

    if (!(modalOverlay && modalWindow)) {
      console.warn(
          '{ModalView} component template contains an error',
      );
      return document.createElement('div');
    }
    this.keyElems = {
      modalOverlay,
      modalWindow,
    };

    return wrapper.firstChild;
  }
}
