import BaseComponent from '../components/_basic/baseComponent.js';

import '../static/img/global/onpointlogo.png';
import '../static/img/global/global.svg';

/**
  * Генерирует HTML-страницу проекта
  * Заполняет блок .content элементами elements в порядке следования
  * @param  {string} title
  * @param  {boolean} containsFeed  // true - logo href = #top to scroll to top
  * + add /back btn to top
  * @param  {...BaseComponent} elements
  * @return {HTMLElement}
  */
export default function createPage(title, ...elements) {
  if (title) {
    pageTitleDiv.className = 'page_title';
    pageTitleDiv.textContent = title;
  }
  const pageDiv = document.createElement('div');
  pageDiv.className = 'page';

  const header = document.createElement('header');
  const headerAnchor = document.createElement('a');
  headerAnchor.dataset['lc_section']='to-main';
  headerAnchor.dataset.router='ignore';
  headerAnchor.href = '/main';
  const headerLogo = document.createElement('img');
  headerLogo.src = '/img/global.svg';

  const footer = document.createElement('footer');
  const footerLogo = document.createElement('img');
  footerLogo.src = '/img/onpointlogo.png';
  footerLogo.className = 'footer__logo';
  const footerSign = document.createElement('span');
  footerSign.innerHTML = '&copy Кириллов Денис';
  footerSign.className = 'footer__sign';

  const contentDiv = document.createElement('div');
  contentDiv.className = 'container';

  elements.forEach((element) => {
    if (element instanceof BaseComponent) {
      contentDiv.appendChild(element.render());
    } else if (element instanceof HTMLElement) {
      contentDiv.appendChild(element);
    } else {
      console.warn(
          '[CreatePage]', element, 'is not BaseComponent nor HTMLElement',
      );
    }
  });

  headerAnchor.appendChild(headerLogo);
  header.appendChild(headerAnchor);
  footer.appendChild(footerLogo);
  footer.appendChild(footerSign);
  pageDiv.appendChild(header);
  pageDiv.appendChild(contentDiv);
  pageDiv.appendChild(footer);

  return pageDiv;
}
