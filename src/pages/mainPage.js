import BasePageMV from './basePageMV.js';
import MainPageView from './mainPageView.js';

import AppLinksController from
  '../components/linksController/appLinksController.js';

// ///////////////////////////////// //
//
//              Main Page
//
// ///////////////////////////////// //

/**
 * @class MainPage
 * @module MainPage
 */
export default class MainPage extends BasePageMV {
  /**
   * @param {HTMLElement} root
   */
  constructor(root) {
    super(root);
    this.moveToMain = () => {};
    this.view = new MainPageView(root);
  }

  show() {
    super.show();
    this.moveToMain = () => this.view.moveToMain();
    AppLinksController.register('to-main', () => this.moveToMain());
  }
}
