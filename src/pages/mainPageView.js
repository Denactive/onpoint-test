import BasePageView from './basePageView.js';
import createPage from './_createPage.js';

import CarouselContent from '../components/carouselContent/carouselContent.js';

// ///////////////////////////////// //
//
//              Main Page
//
// ///////////////////////////////// //

/**
 * @class MainPage
 * @module MainPage
 */
export default class MainPageView extends BasePageView {
  /**
   * @param {HTMLElement} root
   */
  constructor(root) {
    // root не трогать
    super(root);
    this.pageComponents = {
      carousel: new CarouselContent(),
    };
    this.moveToMain = () => {};
  }

  /**
    * Перерисовать главную страницу
    */
  render() {
    super.render();
    this.root.appendChild(createPage('', this.pageComponents.carousel));
    this.moveToMain = () => this.pageComponents.carousel.moveToSlide(1);
  }
}
