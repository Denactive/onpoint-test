'use strict';

import './index.scss';

// pages
import AppModal from './components/modal/appModal.js';
import MainPage from './pages/mainPage.js';

// Controllers
import AppLinksController from
  './components/linksController/appLinksController.js';
import Router from './components/router.js';

console.warn(`
// ///////////////////////////////// //
//       Автор: Кириллов Денис
// ///////////////////////////////// //
`);

AppModal.init();

const root = document.getElementById('root');
const router = new Router(root);

// ///////////////////////////////// //
//
//        Настройки переходов
//
// ///////////////////////////////// //
router
    .register('/', MainPage);

router.start();

AppLinksController.enable();
