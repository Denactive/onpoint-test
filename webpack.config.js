const HTMLWebpackPlugin = require('html-webpack-plugin');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const path = require('path');
const fs = require('fs');
const pug = require('pug');

const PATHS = {
  current: __dirname,
  src: path.join(__dirname, '/src'),
  dist: path.join(__dirname, '/dist'),
  static: path.join(__dirname, '/src/static'),
};
const MODE = process.argv[2]?.replace('--mode=', '') | '';
const DEBUG = MODE === 'development';

console.warn({PATHS, MODE});

// ///////////////////////////////// //
//      Скрипт Сбоорки Кириллов Д.
// ///////////////////////////////// //
//
//   Сохранить <pugFileName>.pug
//   в components/<somewhere>/<pugFileName>.pug.js
//   Если не нужно, чтобы шаблон был сохранен,
//   ставим ''s
//
//   Синтаксис: <pugFileName>: <somewhere>
//   default: <pugFileName>: <pugFileName>
//
// ///////////////////////////////// //

// чтение параметра командной строки

const savePugTo = {
  'template': '',
};

const componentSuffix = 'Component';
const createDirsIfNotExist = false;

console.log(`\n\t===============================
    \t= Скрипт компиляции шаблонов  =
    \t===============================`,
);

/**
 * @param {string} file
 */
function compilePug(file) {
  const base = path.basename(file, '.pug');
  const relative = path.relative(PATHS.src, file);
  const resultRelative = base in savePugTo ?
      savePugTo[base] : relative.replace(`${base}.pug`, '');

  if (resultRelative === '') {
    console.warn(`Шаблон ${base} игнорируется`);
    return;
  }

  // Compile function
  const compiledTemplate = pug.compileFileClient(
      file,
      {
        name: `x(){};\n\nexport default function 
          ${base + componentSuffix}`,
        compileDebug: DEBUG,
      },
  );

  const resultFile = path.join(PATHS.src, resultRelative, base + '.pug.js');

  if (createDirsIfNotExist &&
        !fs.existsSync(path.join(PATHS.src, resultRelative))) {
    fs.mkdirSync(path.join(PATHS.src, resultRelative));
  }

  // Записываем преобразованный паг
  fs.writeFileSync(
      resultFile,
      compiledTemplate,
      'utf8',
  );
  console.log(
      `Шаблон ${base} преобразован в ${path.join(
          resultRelative,
          base + '.pug.js',
      )}`,
  );
}

/**
 * @param {string} dirname
 * @param {callback} walkerFunc
 */
function walkDir(dirname, walkerFunc = (f) => console.log(f)) {
  fs.readdirSync(dirname)
      .map((file) => dirname + '/' + file)
      .forEach((file) => {
        if (fs.statSync(file).isDirectory()) {
          walkDir(file, walkerFunc);
          return;
        }
        if (file.endsWith('.pug')) {
          walkerFunc(file);
        }
      });
}

walkDir(PATHS.src, compilePug);

console.log(`\t===============================\n`);


module.exports = {
  mode: 'production',
  entry: {
    main: path.resolve(__dirname, './src/index.js'),
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
    clean: true,
    assetModuleFilename: 'static/[name][ext][query]',
  },
  optimization: {
    minimize: true,
  },
  // https://webpack.js.org/configuration/dev-server/
  devServer: {
    // To watch multiple static directories:
    static: {
      directory: path.join(__dirname, 'src/static'),
    },
    compress: true,
    port: 8080,
    open: true,
    client: {
      progress: true,
      overlay: {
        errors: true,
        warnings: false,
      },
    },
  },

  plugins: [
    new CopyPlugin({
      patterns: [
        {
          from: path.resolve(PATHS.static, 'img/'),
          to: 'img/',
        },
        {
          from: path.resolve(PATHS.static, 'fonts/'),
          to: 'fonts/',
        },
      ],
    }),
    new HTMLWebpackPlugin({
      filename: 'index.html',
      template: path.resolve(__dirname, './src/index.html'),
      inject: 'body',
      scriptLoading: 'blocking',
    }),
    new CleanWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: '[name].css',
    }),
    // ...pluginsOptions,
    // new HtmlWebpackPugPlugin(),
  ],

  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.(jpe?g|png|gif|svg|ico|webp)$/i,
        type: 'asset/resource',
        generator: {
          filename: 'img/[name][ext][query]',
        },
      },
      {
        test: /\.(ttf|woff|woff2|eot)$/,
        type: 'asset',
        generator: {
          filename: 'fonts/[name][ext][query]',
        },
      },
      {
        test: /\.pug$/,
        loader: 'pug-loader',
      },
      {
        test: /\.(js|ts)x?$/,
        loader: 'babel-loader',
        options: {
          exclude: [
            '/node_modules/',
          ],
        },
      },
    ],
  },
  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    fallback: {
      'fs': false,
    },
    alias: {
      Static: path.resolve(__dirname, 'src/static/'),
    },
  },
};
